import React from 'react'
import { BrowserRouter, Switch, Route } from 'react-router-dom'

import HomePage from '../home/HomePage'
import BlogHome from '../blog/BlogHome'
import SignIn from '../auth/SignIn'
import AdminHome from '../admin/AdminHome'
import { withAuthentication } from '../../session'
import BlogWritePost from '../blog/BlogWritePost'
import BlogPost from '../blog/BlogPost'

const Router = ({authUser}) => (
    <BrowserRouter>
        <Switch>
            <Route exact path='/' component={HomePage} />
            
            {/* Blog */}
            <Route exact path='/blog'  key="blog" component={BlogHome} />
            <Route exact path='/projects'  key="projects" component={BlogHome} />
            <Route exact path='/courses'  key="courses" component={BlogHome} />
            <Route exact path='/discoveries' key="discoveries"  component={BlogHome} />
            <Route exact path='/misc' key="misc" component={BlogHome} />
            <Route exact path='/blog' component={BlogHome} />
            <Route exact path='/blog/post/write' component={BlogWritePost} />
            <Route exact path='/blog/read/:id' component={BlogPost} />

            {/* Authentication */}
            <Route exact path='/auth/signin' component={SignIn} />

            {/* Administration */}
            <Route exact path='/admin' component={AdminHome} />
        </Switch>
    </BrowserRouter>
)

export default withAuthentication(Router)