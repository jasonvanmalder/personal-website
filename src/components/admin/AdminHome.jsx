import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import Loader from '../common/Loader'
import { withAuthorization } from '../../session'

class AdminHome extends Component {
    state = {
        loading: true
    }

    componentDidMount = () => {
        this.setState({loading: false})
    }

    render() {
        if (!this.state.loading) {
            return (
                <section className="full-page">
                    <h1>Administration</h1>
                    <Link to='/blog/post/write'>Écrire un article</Link>
                    <Link to='/blog'>Retour à l'accueil</Link>
                </section>
            )
        } else {
            return (
                <section className="full-page">
                    <Loader />
                </section>
            )
        }
    }
}

export default withAuthorization(authUser => !!authUser)(AdminHome)