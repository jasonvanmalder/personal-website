import React, { Component } from 'react'
import Flip from 'react-reveal/Flip'
import Fade from 'react-reveal/Fade'
import LangService from '../../services/LangService'
import Parallax from './Parallax'
import HomeNav from '../common/HomeNav';
import SchoolImage from '../../assets/img/isf.svg'
import SchoolImage2 from '../../assets/img/heh.svg'
import ukFlag from '../../assets/img/flag/uk.png'
import nederlandsFlag from '../../assets/img/flag/nederlands.png'
import office from '../../assets/img/office-microsoft.png'
import economy from '../../assets/img/economy.png'
import cLogo from '../../assets/img/c.png'
import csharpLogo from '../../assets/img/csharp.png'
import javaLogo from '../../assets/img/java.png'
import centosLogo from '../../assets/img/centos.png'
import wsLogo from '../../assets/img/windows-server.png'
import ciscoLogo from '../../assets/img/cisco.png'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faArrowAltCircleLeft } from '@fortawesome/free-solid-svg-icons'

export default class HomePage extends Component {

    changeQuestionLayout = (layout) => {
        if (this.initialContent) {
            this.initialContent.style.display = 'none'
            this.educationContent.style.display = 'none'
            this.skillsContent.style.display = 'none'

            switch (layout) {
                case 'education':
                    this.educationContent.style.display = 'flex'
                    break;
                case 'skills':
                    this.skillsContent.style.display = 'flex'
                    break;
                default:
                    this.initialContent.style.display = 'flex'
                    break;
            }
        }
    }

    render = () => {
        return (
        <div>
                <HomeNav />
                <Parallax updateParent={e => this.forceUpdate()}/>
                <section className="full-page questions">
                    <div className="plx content">
                        <div className="initialContent" ref={node => this.initialContent = node}>
                            <h1><Flip left cascade delay={200} duration={700}>{LangService.get('hello') + '.'}</Flip></h1>
                            <h2><Flip left cascade delay={1000} duration={700}>{LangService.get('questionsSubtitle')}</Flip></h2>
                            <Fade bottom delay={1500}>
                                <div className='questionChoices'>
                                    <div className='questionChoice' onClick={e =>this.changeQuestionLayout('education')}>
                                        <h3>{LangService.get('education')}</h3>
                                    </div>
                                    <div className='questionChoice' onClick={e =>this.changeQuestionLayout('skills')}>
                                        <h3>{LangService.get('skills')}</h3>
                                    </div>
                                    {/*
                                    <div className='questionChoice' onClick={e => this.changeQuestionLayout('experiences')}>
                                        <h3>Expérience</h3>
                                    </div>
                                    */}
                                </div>
                            </Fade>
                        </div>
                        <div className="educationContent" ref={node => this.educationContent = node} style={{display: 'none'}}>
                            <Fade left>
                                <div className="item">
                                    <Fade top delay={1000}>
                                        <img src={SchoolImage} alt="school" width={200} />
                                    </Fade>
                                    <Fade left delay={1500}>
                                        <div className="title">
                                            <h2>Institut Saint-Ferdinand</h2>
                                            <h3>Technicien de bureau</h3>
                                        </div>
                                    </Fade>
                                    <Flip bottom delay={2000}>
                                        <div className="content">
                                            <img src={ukFlag} alt="english" width={64} />
                                            <img src={nederlandsFlag} alt="nederlands" width={64} />
                                            <img src={office} alt="office" width={128} />
                                            <img src={economy} alt="gestion" width={64} />
                                        </div>
                                    </Flip>
                                </div>
                            </Fade>
                            <Fade right>
                                <div className="item">
                                    <Fade top delay={1000}>
                                        <img src={SchoolImage2} alt="school2" width={200} />
                                    </Fade>
                                    <Fade left delay={1500}>
                                        <div className="title">
                                            <h2>Haute École en Hainaut</h2>
                                            <h3>Bachelier en Informatique &amp; Systèmes option développement</h3>
                                        </div>
                                    </Fade>
                                    <Flip bottom delay={2000}>
                                        <div className="content">
                                                <img src={cLogo} alt="c" width={64} />
                                                <img src={csharpLogo} alt="csharp" width={64} />
                                                <img src={javaLogo} alt="java" width={48} />
                                                <img src={centosLogo} alt="centos" width={128} />
                                                <img src={wsLogo} alt="windows-server" width={128} />
                                                <img src={ciscoLogo} alt="windows-server" width={64} />
                                        </div>
                                    </Flip>
                                </div>
                            </Fade>
                            <span className="backButton" onClick={this.changeQuestionLayout}><FontAwesomeIcon icon={faArrowAltCircleLeft} /></span>           
                        </div>
                        <div className="skillsContent" ref={node => this.skillsContent = node} style={{ display: 'none' }}>
                            <span className="backButton" onClick={this.changeQuestionLayout}><FontAwesomeIcon icon={faArrowAltCircleLeft} /></span>                                               
                        </div>
                    </div>
                    <div className='plx layer-1'></div>
                </section>
        </div>
        )
    }
}
