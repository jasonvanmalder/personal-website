import React, { Component } from 'react'
import Flip from 'react-reveal/Flip'
import LangService from '../../services/LangService'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faGitlab, faLinkedinIn, faInstagram } from '@fortawesome/free-brands-svg-icons'

export default class Parallax extends Component {
    state = {
        textLayerTop: 0,
        secondLayerTop: 200,
        thirdLayerTop: 100
    }

    lastScrollPosition = 0

    handleScroll = () => {
        const secondLayer = this.secondLayer, thirdLayer = this.thirdLayer, textLayer = this.textLayer
        let secondLayerTop = this.state.secondLayerTop, thirdLayerTop = this.state.thirdLayerTop,
                                                textLayerTop = this.state.textLayerTop, newScrollPosition = window.scrollY;
 
        if (newScrollPosition < this.lastScrollPosition){
            secondLayerTop = secondLayerTop + 1.5
            thirdLayerTop = thirdLayerTop + 3
            textLayerTop = textLayerTop + 5
        }else{
            secondLayerTop = secondLayerTop - 1.5
            thirdLayerTop = thirdLayerTop - 3
            textLayerTop = textLayerTop - 5
        }

        if (newScrollPosition > 40) {
            this.setState({ textLayerTop, secondLayerTop, thirdLayerTop })
        } else if(newScrollPosition < 40 && newScrollPosition < this.lastScrollPosition){
            this.setState({ textLayerTop: 0,secondLayerTop: 200, thirdLayerTop: 100 })
        }
        
        this.lastScrollPosition = newScrollPosition;
        
        if (textLayer && secondLayer && thirdLayer) {
            textLayer.style.top =  this.state.textLayerTop + 'px'
            secondLayer.style.top =  this.state.secondLayerTop + 'px'
            thirdLayer.style.top = this.state.thirdLayerTop + 'px'
        }
    }

    setLang = (lang) => {
        localStorage.setItem('lang', lang)
        this.props.updateParent()
        this.forceUpdate()
    }

    componentDidMount = () => {
        const   secondLayer = this.secondLayer,
                     thirdLayer = this.thirdLayer
        
        secondLayer.style.top = this.state.secondLayerTop + 'px'
        thirdLayer.style.top = this.state.thirdLayerTop + 'px'

        window.addEventListener('scroll', this.handleScroll)
    }

    render() {
        return (
            <section className="full-page first" ref={node => this.parallax = node}>
                <div className="plx text" ref={node => this.textLayer = node}>
                    <h1>
                        <Flip left cascade delay={500}>
                            Jason Van Malder
                        </Flip>
                    </h1>
                    <h2>
                    <Flip left cascade delay={1000}>
                        {LangService.get('subtitle')}
                    </Flip>
                    </h2>
                    <div>
                        <span className='plx-icon gitlab'>
                            <Flip bottom delay={2000} duration={1000}>
                                <a href='https://gitlab.com/jasonvanmalder' target='_blank' rel='noopener noreferrer'>
                                    <FontAwesomeIcon icon={faGitlab} />
                                </a>
                            </Flip>
                        </span>
                        <span className='plx-icon instagram'>
                            <Flip bottom delay={2500} duration={1000}>
                                <a href='https://instagram.com/dev_eating_fries' target='_blank' rel='noopener noreferrer'>
                                    <FontAwesomeIcon icon={faInstagram} />
                                </a>
                            </Flip>
                        </span>
                        <span className='plx-icon linkedin'>
                            <Flip bottom delay={3000} duration={1000}>
                                <a href='https://linkedin.com/in/jasonvanmalder' target='_blank' rel='noopener noreferrer'>
                                    <FontAwesomeIcon icon={faLinkedinIn} />
                                </a>
                            </Flip>
                        </span>
                    </div>
                </div>
                <div className='plx layer-1'>
                    <div className='plx layer-2' ref={node => this.secondLayer = node}>
                        <div className='plx layer-3' ref={node => this.thirdLayer = node}></div>
                    </div>
                </div>
                <div className='lang-choice'>
                    <Flip top delay={3500} duration={2000}>
                        <span role='img' aria-label='français' onClick={e => this.setLang('fr')}>🇫🇷</span>
                        <span role='img' aria-label='english' onClick={e => this.setLang('en')}>🇬🇧</span>
                    </Flip>
                </div>
            </section>
        )
    }
}
