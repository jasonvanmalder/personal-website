import React, { Component } from 'react'
import { withFirebase } from '../../firebase'
import { withRouter } from 'react-router-dom'
import { compose } from 'recompose'
import Loader from '../common/Loader'
import 'bootstrap/dist/css/bootstrap.css'
import Fade from 'react-reveal/Fade'

class SignIn extends Component {
    state = {
        loading: true
    }

    componentDidMount = () => {
        setTimeout(() => {
            this.setState({loading: false})
        }, 500)
    }

    render = () => {
        if (!this.state.loading) {
            return (
                <div className="container signin">
                    <Fade>
                        <h1 style={{fontSize: '50px', marginBottom:0}}>Jason Van Malder's Hub</h1>                    
                        <SignInForm />
                    </Fade>
                </div>
            )
        } else {
            return (
                <section className="full-page">
                    <Loader />
                </section>
            )
        }
    }
}

class SignInFormBase extends Component {

    state = {
        email: '',
        password: '',
        error: null
    }

    onChange = e => this.setState({ [e.target.name]: e.target.value })
    
    onSubmit = e => {        
        const { email, password } = this.state

        this.signInButton.innerHTML = 'Chargement...'

        this.props.firebase.doSignIn(email, password)
        .then(authUser => {
            this.props.history.push('/blog')
        })
        .catch(error => {
            switch (error.code) {
                case 'auth/wrong-password':
                    this.setState({ error: 'Mot de passe incorrect !' })
                break
                case 'auth/user-not-found':
                    this.setState({ error: 'Ce compte n\'existe pas !' })
                break
                case 'auth/too-many-requests':
                    this.setState({ error: 'Trop de tentatives, réessayez plus tard !' })
                break
                case 'auth/user-disabled':
                    this.setState({ error: 'Ce compte a été désactivé !' })
                break
                default:
                    this.setState({ error: error.message })
                break
            }

            this.signInButton.innerHTML = 'Connexion'
        })
                
        e.preventDefault()
    }

    componentWillMount = () => {
        this.props.firebase.auth.onAuthStateChanged((user) => {
            if (user) {
                this.props.history.push('/blog')
            }
        })
          
        if (this.props.location.state) {
            const { message } = this.props.location.state
        
            if (message) {
                this.setState({error: message})
            }
        }
    }

    render() {
        const isInvalid = !this.state.email || !this.state.password

        return (
            <form className="form-signin" onSubmit={this.onSubmit} method="POST">
                <h2 className="form-signin-heading">Connexion</h2>
                <div>
                    {this.state.error &&
                        <p>{this.state.error}</p>
                    }
                </div>
                <label htmlFor="inputEmail" className="sr-only">Adresse email</label>
                <input type="email" name="email" className="form-control" placeholder="Adresse email" required autoFocus onChange={this.onChange} />
                <label htmlFor="inputPassword" className="sr-only">Mot de passe</label>
                <input type="password" name="password" className="form-control" placeholder="Mot de passe" required onChange={this.onChange} />
                <button disabled={isInvalid} className="btn btn-lg btn-primary btn-block signin" type="submit" ref={node => this.signInButton = node}>Connexion</button>
            </form>
        )
    }

}

const SignInForm = compose(withRouter,withFirebase)(SignInFormBase)

export default SignIn
export {SignInForm}