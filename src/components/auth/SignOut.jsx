import React from 'react'
import { withFirebase } from '../../firebase'
import { withRouter } from 'react-router-dom'
import { compose } from 'recompose'

const SignOutBase = ({ firebase, history }) => {
    const signOutAndRedirect = () => {
        firebase.doSignOut()
        .then(() => {
            history.push({pathname: '/auth/signin', state: {message: 'Vous avez bien été déconnecté !'}})
        })
        .catch(err => {
            console.log(err)
        })
    }

    return (
        <span onClick={signOutAndRedirect}>Déconnexion</span>
    )
}

const SignOut = compose(withRouter,withFirebase)(SignOutBase)

export default SignOut