import React, { Component } from 'react'
import Fade from 'react-reveal/Fade'
import BlogNavbar from './BlogNavbar'
import DateService from '../../services/DateService'
import ReactMarkdown from 'react-markdown'
import CodeBlock from '../common/CodeBlock'
import { compose } from 'recompose'
import { AuthUserContext } from '../../session'
import { withFirebase } from '../../firebase'
import { withRouter } from 'react-router-dom'
import Loader from '../common/Loader'

class BlogPostBase extends Component {

    state = {
        loading: true
    }

    componentWillMount = () => {
        this.props.firebase.post(this.props.match.params.id).on('value', snapshot => {
            this.setState({ ...snapshot.val(), loading: false })
            if (!snapshot.val()) {
                this.props.history.push('/blog')
            }
        })
    }

    getCollections = () => {
        let collections = ''
        let id = 0

        if (this.state.collections) {
            this.state.collections.forEach(collection => {
                if(id > 0) collections += " et "
                collections += collection
                id++
            })
        } else {
            collections = 'Aucune'
        }

        return collections
    }

    render() {
        const { authUser } = this.props

        if (!this.state.loading && this.state.title) {
            return (
                <div>
                    <BlogNavbar authUser={ authUser } backLocation="/blog" />
                    <Fade bottom>
                        <div className="container">
                            <div className="row post-write">
                                <div className="col-md-11 well">
                                    <div className="header">
                                        <h1>{ this.state.title }</h1>
                                        <span className="date">{ DateService.convertFromTimestamp(Date.now()) } </span>
                                        <span className="collections">— { this.getCollections() }</span>
                                    </div>
                                    <img className="img" src={ this.state.image } alt="article-img" />
                                    <ReactMarkdown
                                        source={ this.state.content }
                                        renderers={{ code: CodeBlock }}
                                        escapeHtml={ false }
                                        skipHtml={ false }
                                    />
                                </div>
                            </div>
                        </div>
                    </Fade>
                </div>
            )
        } else {
            return (
                <section className="full-page first">
                    <Loader />
                </section>
            )
        }
    }
}

const BlogPost = compose(withFirebase, withRouter)(BlogPostBase)

const BlogPostFinal = () => (
    <AuthUserContext.Consumer>
        {authUser =>
            <BlogPost authUser={authUser} />
        }
    </AuthUserContext.Consumer>
)

export default BlogPostFinal