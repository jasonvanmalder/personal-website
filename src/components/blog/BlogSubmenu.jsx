import React, { Component } from 'react'
import { NavLink } from 'react-router-dom'

export default class BlogSubmenu extends Component {
  render() {
    return (
        <div className="submenu">
            <ul>
                <li><NavLink to="/blog" activeClassName="active">Tous</NavLink></li>
                <li><NavLink to="/projects" activeClassName="active">Projets</NavLink></li>
                <li><NavLink to="/courses" activeClassName="active">Tutoriels</NavLink></li>
                <li><NavLink to="/discoveries" activeClassName="active">Découvertes</NavLink></li>
                <li><NavLink to="/misc" activeClassName="active">Autres</NavLink></li>
            </ul>
        </div>
    )
  }
}
