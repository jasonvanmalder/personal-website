import React, { Component } from 'react'
import { AuthUserContext } from '../../session'
import Loader from '../common/Loader'
import BlogNavbar from './BlogNavbar'
import BlogSubmenu from './BlogSubmenu'
import Fade from 'react-reveal/Fade'
import '../../assets/css/blog/posts.css'
import BlogPostThumbnail from './BlogPostThumbnail'
import { withFirebase } from '../../firebase'
import { compose } from 'recompose'
import { withRouter } from 'react-router-dom'

class BlogHomeBase extends Component {

    state = {
        loading: true,
        posts: null
    }

    componentWillMount = () => {
        let posts = []
        
        this.props.firebase.posts().on('value', snapshot => {
            posts = []
            this.setState({loading: true})

            let dbPosts = snapshot.val()
            
            if (dbPosts) {
                Object.keys(dbPosts).forEach(key => {
                    let post = dbPosts[key]
                    post.id = key
                    posts.push(post)
                })
    
                posts.reverse()

                let displayedPost = []

                const checkCollection = (collection) => {
                    switch (this.props.location.pathname) {
                        case '/projects':
                            return collection === 'Projets'
                        case '/courses':
                            return collection === 'Tutoriels'
                        case '/discoveries':
                            return collection === 'Découvertes'
                        case '/misc':
                            return collection === 'Autres'
                        default:
                            return true
                    }
                }

                posts.forEach(post => {
                    const collectionMeetRequirements = post.collections.some(checkCollection)
                    if(collectionMeetRequirements) displayedPost.push(post)
                })
    
                const rows = [...Array(Math.ceil(displayedPost.length / 3))]
                const postsRows = rows.map((row, idx) => displayedPost.slice(idx * 3, idx * 3 + 3))
                const postsList = postsRows.map((row, idx) => (
                    <div className="row" key={idx}>    
                        {
                            row.map(post =>
                                <BlogPostThumbnail key={post.id} {...post} />
                            )
                        }
                    </div>
                ))
    
                this.setState({ posts: postsList })
            }

            this.setState({ loading: false })
        })
    }

    render() {
        const { authUser } = this.props

        if (!this.state.loading) {
            return (
                    <div className="bg-image">
                        <BlogNavbar authUser={authUser} backLocation="/" />
                        <BlogSubmenu />
                        <Fade>
                            <div className="container">
                                <div className="posts">
                                    {(this.state.posts && this.state.posts.length > 0) ? this.state.posts :
                                        <div style={{width: '100%', height: '100%', textAlign: 'center'}}><h2>Il n'y a pas d'article pour le moment</h2></div>
                                    }
                                </div>
                            </div>
                        </Fade>
                    </div>
            )
        } else {
            return (
                <section className="full-page first">
                    <Loader />
                </section>
            )
        }
    }

}

const BlogHome = compose(withFirebase, withRouter)(BlogHomeBase)

const BlogHomeFinal = () => (
    <AuthUserContext.Consumer>
        {authUser =>
            <BlogHome authUser={authUser}/>
        }
    </AuthUserContext.Consumer>
)

export default BlogHomeFinal