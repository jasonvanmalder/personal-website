import React, { Component } from 'react'
import DateService from '../../services/DateService'
import { withRouter } from 'react-router-dom'

class BlogPostThumbnail extends Component {

    redirect = () => {
        this.props.history.push(`/blog/read/${this.props.id}`)
    }

    render = () => {
        const { id, image, date, title, introduction, collections } = this.props

        return (
        <article className="col-md-4">
            <div className="post" onClick={this.redirect}>
                <div className="image">
                    <img src={image} alt={`${id}`} />
                </div>
                <div className="infos">
                    <span className="date">{ DateService.convertFromTimestamp(date) }</span>
                    <h2>{title}</h2>
                    <p>{introduction}</p>
                </div>
                <div className="collections">
                    { (collections) ?
                        collections.map((collection, idx) => {
                            collection = collection.charAt(0).toUpperCase() + collection.slice(1)
                            return <span key={idx}>{(idx > 0) ? ' | ' + collection : collection}</span>
                        }) : <span>Aucune</span>
                    }
                </div>
            </div>
        </article>
        )
    }
}

export default withRouter(BlogPostThumbnail)