import React, { Component } from 'react'
import { AuthUserContext, withAuthorization } from '../../session'
import Fade from 'react-reveal/Fade'
import BlogNavbar from './BlogNavbar'
import { FormGroup, Label, Input, Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap'
import '../../assets/css/blog/postWrite.css'
import DateService from '../../services/DateService'
import ReactMarkdown from 'react-markdown'
import CodeBlock from '../common/CodeBlock'
import { withFirebase } from '../../firebase'
import { compose } from 'recompose'

class BlogWritePostBase extends Component {

    state = {
        writingModal: false,
        postTitle: '',
        postIntroduction: '',
        postContent: '',
        postImage: '',
        postCollections: []
    }
    
    onChange = (e) => {
        if (e.target.name !== 'postCollections') {
            this.setState({ [e.target.name]: e.target.value })
            localStorage.setItem(e.target.name, e.target.value)
            if (!localStorage.getItem(e.target.name)) {
                localStorage.removeItem(e.target.name)
            }
        } else {
            let options = e.target.options
            let postCollections = []

            for (let i = 0, l = options.length; i < l; i++) {
                if (options[i].selected) {
                    postCollections.push(options[i].value)
                }
            }

            this.setState({ postCollections })
        }
    }

    publish = () => {
        let post = {
            title: this.state.postTitle,
            introduction: this.state.postIntroduction,
            content: this.state.postContent,
            image: this.state.postImage,
            collections: this.state.postCollections,
            date: Date.now()
        }

        this.props.firebase.posts().push(post)
        this.toggleWritingModal()
    }

    toggleWritingModal = () => {
        this.setState({ writingModal: !this.state.writingModal })
    }

    clearStorageAndState = () => {
        this.setState({
            postTitle: '',
            postIntroduction: '',
            postImage: '',
            postContent: '',
            postCollections: []
        })

        localStorage.removeItem('postTitle')
        localStorage.removeItem('postIntroduction')
        localStorage.removeItem('postImage')
        localStorage.removeItem('postContent')
    }

    getCollections = () => {
        let collections = ''
        let id = 0

        this.state.postCollections.forEach(collection => {
            if(id > 0) collections += " et "
            collections += collection
            id++
        })

        return collections
    }

    componentWillMount = () => {
        this.setState({
            postTitle: localStorage.getItem('postTitle'),
            postIntroduction: localStorage.getItem('postIntroduction'),
            postImage: localStorage.getItem('postImage'),
            postContent: localStorage.getItem('postContent')
        })
    }

    render() {
        const { authUser } = this.props
        return (
            <Fade>
                <Modal isOpen={this.state.writingModal} toggle={this.toggleWritingModal} className="post-write-modal">
                    <ModalHeader>Écrire un article</ModalHeader>
                    <ModalBody>
                        <FormGroup>
                            <Label for="postTitle">Titre de l'article (! pour passer à la ligne)</Label>
                            <Input type="text" name="postTitle" placeholder="Titre de l'article" value={this.state.postTitle} onChange={this.onChange} />
                        </FormGroup>
                        <FormGroup>
                            <Label for="postIntroduction">Texte d'intro pour la miniature</Label>
                            <Input type="text" name="postIntroduction" placeholder="Introduction" value={this.state.postIntroduction} onChange={this.onChange} />
                        </FormGroup>
                        <FormGroup>
                            <Label for="postImage">Image en avant de l'article</Label>
                            <Input type="text" name="postImage" placeholder="URL" value={this.state.postImage} onChange={this.onChange} />
                        </FormGroup>
                        <FormGroup>
                            <Label for="postContent">Contenu de l'article</Label>
                            <Input type="textarea" name="postContent" placeholder="Écrivez l'article ici (en markdown)" value={this.state.postContent} onChange={this.onChange} />
                        </FormGroup>
                        <FormGroup>
                            <Label for="exampleSelectMulti">Catégorie(s)</Label>
                            <Input type="select" name="postCollections" onChange={this.onChange} multiple>
                                <option>Projets</option>
                                <option>Tutoriels</option>
                                <option>Découvertes</option>
                                <option>Autres</option>
                            </Input>
                        </FormGroup>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="primary" onClick={ this.publish }>Publier</Button>{' '}
                        <Button color="secondary" onClick={ this.clearStorageAndState }>Vider tous les champs</Button>{' '}
                        <Button color="secondary" onClick={ this.toggleWritingModal }>Fermer</Button>
                    </ModalFooter>
                </Modal>
                <BlogNavbar authUser={ authUser } backLocation="/blog" toggleWritingModal={ this.toggleWritingModal } />
                <div className="container">
                    <div className="row post-write">
                        <div className="col-md-11 well">
                            <div className="header">
                                <h1>{ this.state.postTitle }</h1>
                                <span className="date">{ DateService.convertFromTimestamp(Date.now()) } </span>
                                <span className="collections">— { this.getCollections() }</span>
                            </div>
                            <img className="img" src={ this.state.postImage } alt="article-img" />
                            <ReactMarkdown
                                source={ this.state.postContent }
                                renderers={{ code: CodeBlock }}
                                escapeHtml={ false }
                                skipHtml={ false }
                            />
                        </div>
                    </div>
                </div>
            </Fade>
        )
  }
}

const BlogWritePost = compose(withFirebase)(BlogWritePostBase)

const BlogWritePostFinal = () => (
    <AuthUserContext.Consumer>
        {authUser =>
            <BlogWritePost authUser={authUser} />
        }
    </AuthUserContext.Consumer>
)


export default withAuthorization(authUser => !!authUser)(BlogWritePostFinal)