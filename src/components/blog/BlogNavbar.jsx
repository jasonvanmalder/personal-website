import React from 'react'
import { Link, withRouter } from 'react-router-dom'
import SignOut from '../auth/SignOut'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faArrowAltCircleLeft } from '@fortawesome/free-solid-svg-icons'
import '../../assets/css/blog/nav.css'

class BlogNavbar extends React.Component {

    goHome = () => {
        this.props.history.push('/')
    }

    toggleWritingModal = (e) => {
        e.preventDefault();
        this.props.toggleWritingModal();
    }

    render = () => {
        const { authUser } = this.props
        const url = this.props.history.location.pathname,
        
        notAuthenticatedMenu = (
            <li>
                <Link to="/auth/signin">Connexion</Link>
            </li>
        ),
        
        writePost = (url === '/blog/post/write' || url === '/blog/post/write/') ?
            (<li><Link to='/blog/post/write' onClick={this.toggleWritingModal}>Ouvrir la fenêtre d'écriture</Link></li>) :
            (<li><Link to ='/blog/post/write'>Écrire un article</Link></li>),
        
        authenticatedMenu = (
            <div>
                {writePost}
                <li>
                    <Link to="/admin">Administration</Link>
                </li>
                <li>
                    <Link to="/blog" onClick={e => e.preventDefault()}><SignOut /></Link>
                </li>
            </div>        
        )

        return (
            <nav className="blog-navbar">
                <ul>
                    <div className="left">
                        <li className="blogBackButton">
                            <Link to={this.props.backLocation}><FontAwesomeIcon icon={faArrowAltCircleLeft} /></Link>
                        </li>
                    </div>
                    <div className="right">
                        { authUser ? authenticatedMenu : notAuthenticatedMenu }
                        <li className="newsletterButton">
                            <Link to="/">Newsletter</Link>
                        </li>
                    </div>
                </ul>
            </nav>
        )
    }
}

export default withRouter(BlogNavbar)