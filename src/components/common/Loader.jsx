import React from 'react'
import loader from '../../assets/img/loader.svg'

const Loader = () => (
    <img src={loader} alt="Loader" />
)

export default Loader;