import React, { Component } from 'react'
import {Link} from 'react-router-dom'
import Fade from 'react-reveal/Fade'
import LangService from '../../services/LangService'

export default class HomeNav extends Component {
  render() {
    return (
        <Fade top delay={1000}>
            <nav className="navbar">
                <ul>
                    <div className="items">
                        <Fade top delay={1500}><li><Link to='/blog'>{LangService.get('blog')}</Link></li></Fade>
                        <Fade top delay={1600}><li><Link to='/'>{LangService.get('projects')}</Link></li></Fade>
                        <Fade top delay={1700}><li><Link to='/'>{LangService.get('portfolio')}</Link></li></Fade>
                        <Fade top delay={1800}><li><Link to='/'>{LangService.get('contact')}</Link></li></Fade>
                    </div>
                </ul>
            </nav>
        </Fade>
    )
  }
}
