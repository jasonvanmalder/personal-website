export default class DateService {
    static convertFromTimestamp = (timestamp) => {
        const months = ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre']
        const date = new Date(timestamp)
        return  `Le ${date.getDate()} ${months[date.getMonth()]} ${date.getFullYear()}`
    }
}