export default class LangService {

    static load = () => {
        const availableLanguages = ['fr', 'en']
        let lang

        if (!localStorage.getItem('lang')) {
            localStorage.setItem('lang', 'fr')
            lang = require('../assets/langs/fr')
        } else {
            const storageLang = localStorage.getItem('lang')
            if (availableLanguages.includes(storageLang)) {
                lang = require('../assets/langs/' + storageLang)
            } else {
                lang = require('../assets/langs/fr')
            }
        }

        return lang
    }

    static get = (key) => {
        const lang = LangService.load().default
        return lang[key]
    }

}