const lang = {
    'blog': 'Blog',
    'projects': 'Projets',
    'portfolio': 'Portfolio',
    'contact': 'Contact',
    'subtitle': 'Développeur Web Full-Stack',
    'hello': 'Bonjour',
    'questionsSubtitle': 'Que voulez-vous savoir à propos de moi ?',
    'education': 'Parcours scolaire',
    'skills': 'Compétences'
}

export default lang