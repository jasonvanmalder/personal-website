const lang = {
    'blog': 'Blog',
    'projects': 'Projects',
    'portfolio': 'Portfolio',
    'contact': 'Contact',
    'subtitle': 'Full-Stack Web Developer',
    'hello': 'Hello',
    'questionsSubtitle': 'What would you like to know about me?',
    'education': 'Education',
    'skills': 'Skills'
}

export default lang